/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gatethreads;

import org.lorecraft.phparser.*;
/**
 *
 * @author razlet
 */
public class GateThreads implements Runnable{
    public String type;
    public String request;
    
    public GateThreads(String _type, String _request){
        type = _type;
        request = _request;
    }
    
    public void run(){
        String example = request;
        SerializedPhpParser serializedPhpParser = new SerializedPhpParser(example);
        Object _request = serializedPhpParser.parse();
        /*Начнем работать с запросом,
         пока только разобьем на составляющие*/
        System.out.println(_request);
    }
}
