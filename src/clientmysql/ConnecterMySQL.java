/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clientmysql;

import java.util.Map;
import java.util.Iterator;
import java.util.HashMap;
import java.util.Vector;

import java.util.List;

//Для работы с mysql
import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

/**
 *
 * @author razlet
 */
public class ConnecterMySQL {

    protected String password = "wL9v4yJ8wXMajy3M";
    protected String root = "root";
    protected String table = "gate";
    protected String url = "jdbc:mysql://razlet.ru/gate?zeroDateTimeBehavior=convertToNull";
    protected Connection con;
    protected Statement stmp = null;

    public ConnecterMySQL() {
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            DriverManager.setLoginTimeout(15);
            con = DriverManager.getConnection(url, root, password);
            stmp = con.createStatement();
        } catch (Exception e) {
            System.out.println("Error connection " + e);
        }
    }

    public ConnecterMySQL(String _url, String _root, String _pw) {
        try {
            password = _pw;
            url = _url;
            root = _root;
            con = DriverManager.getConnection(url, root, password);
            stmp = con.createStatement();
            
            
        } catch (Exception e) {
            System.out.println("Error connection " + e);
        }
    }

    public int setNewConnection(String _url, String _root, String _pw) {
        try {
            password = _pw;
            url = _url;
            root = _root;
            con = DriverManager.getConnection(url, root, password);
            stmp = con.createStatement();
        } catch (Exception e) {
            System.out.println("Error connection " + e);
        }
        return 1;
    }

    public Connection getConnection() {
        return con;
    }

    public int insert_row(Map<String, String> data, String table) throws java.sql.SQLException {
        String keyes = "";
        String values = "";
        String sql = "";
        Iterator it = data.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pairs = (Map.Entry) it.next();//Следующий элемент
            //Формируем поля для бд
            if (pairs.getValue() != null) {
                values += "'" + pairs.getValue() + "'";
            } else {
                values += "'" + pairs.getValue() + "'";
            }
            keyes += "`" + pairs.getKey() + "`";
            if (it.hasNext()) {
                values += ",";
                keyes += ",";
            }

            it.remove(); // avoids a ConcurrentModificationException
        }
        //Вставляем
        sql = "INSERT INTO  `" + table + "` ( " + keyes + " ) VALUES ( " + values + " )";
        System.out.println(sql);

        Statement stmt = con.createStatement();//Интересно, что не срабатывает с переменной в классе
        stmt.executeUpdate(sql, Statement.RETURN_GENERATED_KEYS);//Делаем запрос и получаем id
        ResultSet rs = stmt.getGeneratedKeys();

        if (rs.next()) {
            return rs.getInt(1);
        } else {
            return -1;
        }

    }

    /**
     * Изменяет запись в БД
     *
     * @param data
     * @param table
     * @param type
     * @param where
     * @param compress
     * @throws java.sql.SQLException
     */
    public void changeRow(String type, Map<String, String> data, String table, String where, List<String> compress) throws java.sql.SQLException {
        /*
         * Объект вставлялка
         */
        String keyes = "";
        String values = "";
        String sql = "";
        Statement stmt = null;
        Iterator it = null;

        //Для сжатия
        String borderLeft = "'";
        String borderRight = "'";

        //Значение внутри массива
        String value = "";
        String key = "";
        
        
        it = data.entrySet().iterator();
        
        values = "";
        while (it.hasNext()) {
            
            Map.Entry pairs = (Map.Entry) it.next();//Следующий элемент

            value = pairs.getValue().toString();
            key = pairs.getKey().toString();
            
            
            //Проверяем на наличие
            if(compress!=null){
                
                if (compress.indexOf(key) != -1) {
                    borderLeft = "COMPRESS(\"";
                    borderRight = "\")";
                }else{
                    borderLeft = "\"";
                    borderRight = "\"";
                }
            }
            
            //Определяем тип действия
            switch (type) {
                case "insert"://Втсавить текст

                    //Формируем поля для бд
                    values += borderLeft + value + borderRight;

                    keyes += "`" + key + "`";
                    if (it.hasNext()) {
                        values += ",";
                        keyes += ",";
                    }
                    
                    it.remove(); 

                    //Вставляем
                    sql = "INSERT INTO  `" + table + "` ( " + keyes + " ) VALUES ( " + values + " )";

                    break;
                case "update":
                    
                    values += key + "=" + borderLeft + value + borderRight;
                    
                    if (it.hasNext()) {
                        values += ", ";
                    }
                    it.remove(); // avoids a ConcurrentModificationException

                
                    //формируем запрос
                    sql = "UPDATE  `" + table + "` SET " + values + " WHERE " + where;
                    
                
                break;
            }
        
        }
        System.out.println(sql);
            
        //Если пустая строка 
        if(sql.equals(""))
            return;
        
        stmt = con.createStatement();
        stmt.executeUpdate(sql);
    }

    public void insert(Map<String, String> data, String table, List<String> compress) throws java.sql.SQLException {
        this.changeRow("insert", data, table, "", compress);
    }

    public void insert(Map<String, String> data, String table) throws java.sql.SQLException {
        this.changeRow("insert", data, table, "", null);
    }
    
    public void update(Map<String, String> data, String table, String where, List<String> compress) throws java.sql.SQLException {
        this.changeRow("update", data, table, where, compress);
    }

    public void update(Map<String, String> data, String table) throws java.sql.SQLException {
        this.changeRow("update", data, table, "", null);
    }
    
    public Vector<Map> query(String req) throws java.sql.SQLException {
        ResultSet res = stmp.executeQuery(req);
        Vector<Map> result = new Vector<Map>();


        // System.out.println(res.getConcurrency());

        while (res.next()) {
            ResultSetMetaData rsmd = res.getMetaData();

            Map<String, String> data = new HashMap<String, String>();
            for (int i = 1; i <= rsmd.getColumnCount(); i++) {

                if (!data.containsKey(rsmd.getColumnName(i))) {
                    data.put(rsmd.getColumnName(i), res.getString(i));
                }

            }
            result.add(data);


            //System.out.println(str);
        }

        return result;
    }

    public void destroyConnection() {
        if (con != null) {
            try {
                con.close();
                System.out.println("Connection is killed ");
            } catch (Exception e) {
                System.out.println("Error connection " + e);
            }
        }
    }
}
